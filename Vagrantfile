$cpus = 8
$memory = 8192
$ip = "172.18.13.101"

$script = <<-SCRIPT
export LC_ALL="en_US.UTF-8"

# elastic
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-6.x.list
apt-get update -y
apt-get install -y openjdk-8-jdk
apt-get install -y elasticsearch
apt-get install -y kibana
apt-get install -y logstash

echo "network.host: 0.0.0.0" >> /etc/elasticsearch/elasticsearch.yml
echo "server.host: 0.0.0.0" >> /etc/kibana/kibana.yml
cp /vagrant/logstash.conf /etc/logstash/conf.d/logstash.conf
cp /vagrant/logs-template.json /etc/logstash/logs-template.json

systemctl daemon-reload
systemctl enable elasticsearch.service
systemctl start elasticsearch.service
systemctl enable kibana.service
systemctl start kibana.service
systemctl enable logstash.service
systemctl start logstash.service

# docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update -y
apt-get install -y docker-ce
usermod -aG docker vagrant

# kafka
docker run -d -p 2181:2181 -p 9092:9092 --env ADVERTISED_HOST=#{$ip} --env ADVERTISED_PORT=9092 spotify/kafka
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/xenial64"
  config.vm.provider "virtualbox" do |v|
    v.memory = $memory
    v.cpus = $cpus
  end
  config.vm.network "private_network", ip: $ip
  config.vm.network "forwarded_port", guest: 5601, host: 5601
  config.vm.provision "shell", inline: $script
end
